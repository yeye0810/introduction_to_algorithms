def whole_sortArray(array,p,q):
    result = merge_sort(array,p,q)
    array[p:q] = result
    return array
def merge_sort(array, p, q):
    if p == q-1:
        return [array[p]]#为了保证即使只有一个数也是一个list
    mid = (p+q)//2
    left = merge_sort(array, p, mid)
    right = merge_sort(array, mid, q)
    return merge(left, right)
def merge(left, right):
    result = []
    len_left = len(left)
    len_right = len(right)
    i=0
    j=0
    while i <len_left and j < len_right:
        if left[i] < right[j]:
            result.append(left[i])
            i+=1
        else:
            result.append(right[j])
            j+=1
    #将剩下的加到数组中
    result+=left[i:]
    result+=right[j:]
    return result
if __name__ == '__main__':
    array = [1,6,2,8,2,10,14,13,12]
    print(whole_sortArray(array,4,len(array)))