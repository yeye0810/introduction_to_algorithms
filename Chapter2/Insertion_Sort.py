def insert_sort(arrayA):
    '''

    :param arrayA:待排序数组
    :return: 排序数组
    算法：取出一个值作为key，如果key比前一个数小，那么说明key应该在前一个数的前面，那么前一个数应该在key的位置，因此将前一个数赋值到key的位置
    当第i-1个数小于key时，那么说明key的位置在第i个上，那么将i位置上的值换为key，原本i位置上的值在i+1处
    '''
    for j in range(1,len(arrayA)):
        key = arrayA[j]
        i = j - 1
        while i >= 0 and arrayA[i] > key:
            arrayA[i+1] = arrayA[i]
            i = i - 1
        arrayA[i+1] = key
    return arrayA
if __name__ == '__main__':
    arrayA = [1,5,2,0,6,10]
    print(insert_sort(arrayA))