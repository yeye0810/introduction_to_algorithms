def findMaxSubarray(array,begin,end):
    '''

    :param array: The array of enter
    :param begin: minimum of item
    :param end: maxumum of item
    :return: the max_sum value and begin item and end item getting the max_sum
    '''
    if begin == end:
        return (begin, end, array[begin])
    sum_value = 0
    left = 0
    right = 0
    max_sum_value = -float('inf')
    for i in range(begin, end+1):
        sum_value = 0
        for j in range(i, end+1):
            sum_value += array[j]
            if sum_value > max_sum_value:
                max_sum_value = sum_value
                left = i
                right = j
    return (left, right, max_sum_value)
if __name__ == '__main__':
    A = [1, -4, 3, -4]
    print(findMaxSubarray(A, 0, 3))