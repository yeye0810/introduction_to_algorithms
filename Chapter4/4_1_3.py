import DivideAndCombine as Div
import Exhaustionofviolence as Exh
from time import  time
if __name__ == '__main__':
    num_list=2
    _flag = True
    while (_flag):
        array = list(range(num_list))
        start1 = time()
        Div.FindMaximumSubarray(array,0,num_list-1)
        end1 = time()
        runtime1 = end1 - start1
        start2 = time()
        Exh.findMaxSubarray(array,0,num_list-1)
        end2 = time()
        runtime2 = end2 - start2
        print('Runing time 1 is {} and Running time 2 is {}'.format(runtime1,runtime2))
        if runtime2 > runtime1:
            _flag = False
        if num_list == 100:
            _flag = False
        num_list += 1
    print(num_list)


